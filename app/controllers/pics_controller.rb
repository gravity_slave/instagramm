class PicsController < ApplicationController
  before_action :set_pic, only: [:show,:edit,:update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @pics = Pic.all.order('created_at DESC')
  end

  def show

  end
  def new
    @pic = current_user.pics.create
  end

  def create
    @pic = current-user.pics.build(pic_params)
    if @pic.save
      redirect_to @pic
      flash[:success] = 'pic was successfully created'
    else
      render 'new'

    end
  end

  def edit

  end

  def update
    if @pic.update_attributes(pic_params)
      redirect_to pic_path(@pic)
      flash[:success] = " You have successfully updated #{@pic.title} !"
    else
      render 'edit'
    end
  end

  def destroy
     @pic.destroy
      redirect_to pics_path
      flash[:notice] = " You have deleted this pic"



  end

  private
  def pic_params
    params.require(:pic).permit(:title, :description, :image)
  end

  def set_pic
     @pic = Pic.find(params[:id])

  end
end
